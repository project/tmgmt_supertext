<?php

namespace Drupal\tmgmt_supertext_test;

use Drupal\tmgmt_supertext\Plugin\tmgmt\Translator\SupertextTranslator;
use GuzzleHttp\Promise\FulfilledPromise;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\RequestInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Guzzle middleware for the Supertext API.
 */
class SupertextTestMiddleware {

  /**
   * Invoked method that returns a promise.
   */
  public function __invoke() {
    return function ($handler) {
      return function (RequestInterface $request, array $options) use ($handler) {
        $uri = $request->getUri();

        // Intervene if we are testing supertext.
        $requested_url = $uri->getScheme() . '://' . $uri->getHost();
        if (in_array("$requested_url/api/", [SupertextTranslator::API_URL, SupertextTranslator::API_URL])) {
          return $this->createRemotePromise($request);
        }

        // Otherwise, no intervention. We defer to the handler stack.
        return $handler($request, $options);
      };
    };
  }

  /**
   * Creates a promise for the supertext request.
   *
   * @param \Psr\Http\Message\RequestInterface $request
   *   The request.
   *
   * @return \GuzzleHttp\Promise\PromiseInterface
   *   The promise.
   */
  protected function createRemotePromise(RequestInterface $request) {
    $uri = $request->getUri();
    $filtered_parts = array_filter(explode('/', $uri->getPath()), function ($part) {
      if (in_array($part, ['', 'api', 'v1', 'v1.1'])) {
        return FALSE;
      }
      return TRUE;
    });
    $path_parts = array_values($filtered_parts);

    if ($path_parts[0] == 'translation' && $path_parts[1] == 'languagemapping') {
      return new FulfilledPromise($this->languageMapping($path_parts[2]));
    }
    if ($path_parts[0] == 'accountcheck') {
      return new FulfilledPromise($this->accountCheck($request));
    }
    if ($path_parts[0] == 'translation' && $path_parts[1] == 'quote') {
      return new FulfilledPromise($this->quote());
    }
    if ($path_parts[0] == 'translation' && $path_parts[1] == 'order') {
      return new FulfilledPromise($this->order($request));
    }
  }

  /**
   * Returns language mapping JSON as Supertext would return it.
   */
  protected function languageMapping($langcode) {
    switch ($langcode) {
      case 'de':
        $data = '{"Supported":false,"Languages":[{"Code":"de-AT","IsPriorityLanguage":true,"IsSystemLanguage":true,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"de-AT","SubLanguageOf":21,"Id":14,"Title":"German (Austria)"},{"Code":"de-DE","IsPriorityLanguage":true,"IsSystemLanguage":true,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"de-DE","SubLanguageOf":21,"Id":13,"Title":"German (Germany)"},{"Code":"de-CH","IsPriorityLanguage":true,"IsSystemLanguage":true,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"de-CH","SubLanguageOf":21,"Id":1,"Title":"German (Switzerland)"}]}';
        break;

      case 'ru':
        $data = '{"Supported":false,"Languages":[{"Code":"ru-RU","IsPriorityLanguage":false,"IsSystemLanguage":false,"EnabledTargetLanguage":true,"EnabledSourceLanguage":true,"SdlCode":"ru-RU","SubLanguageOf":null,"Id":12,"Title":"Russian"}]}';
        break;

      case 'it':
        $data = '{"Supported":false,"Languages":[{"Code":"it-IT","IsPriorityLanguage":true,"IsSystemLanguage":true,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"it-IT","SubLanguageOf":24,"Id":17,"Title":"Italian (Italy)"},{"Code":"it-CH","IsPriorityLanguage":true,"IsSystemLanguage":true,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"it-CH","SubLanguageOf":24,"Id":4,"Title":"Italian (Switzerland)"}]}';
        break;

      case 'es':
        $data = '{"Supported":false,"Languages":[{"Code":"es-AR","IsPriorityLanguage":false,"IsSystemLanguage":false,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"es-AR","SubLanguageOf":51,"Id":111,"Title":"Spanish (Argentina)"},{"Code":"es-CL","IsPriorityLanguage":false,"IsSystemLanguage":false,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"es-CL","SubLanguageOf":51,"Id":97,"Title":"Spanish (Chile)"},{"Code":"es-CO","IsPriorityLanguage":false,"IsSystemLanguage":false,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"es-CO","SubLanguageOf":51,"Id":112,"Title":"Spanish (Colombia)"},{"Code":"es-419","IsPriorityLanguage":false,"IsSystemLanguage":false,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"es-419","SubLanguageOf":51,"Id":48,"Title":"Spanish (Latin-American)"},{"Code":"es-MX","IsPriorityLanguage":false,"IsSystemLanguage":false,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"es-MX","SubLanguageOf":51,"Id":84,"Title":"Spanish (Mexico)"},{"Code":"es-PE","IsPriorityLanguage":false,"IsSystemLanguage":false,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"es-PE","SubLanguageOf":51,"Id":98,"Title":"Spanish (Peru)"},{"Code":"es-ES","IsPriorityLanguage":false,"IsSystemLanguage":false,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"es-ES","SubLanguageOf":51,"Id":8,"Title":"Spanish (Spain)"},{"Code":"es-US","IsPriorityLanguage":false,"IsSystemLanguage":false,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"es-US","SubLanguageOf":51,"Id":83,"Title":"Spanish (USA)"}]}';
        break;

      case 'en':
        $data = '{"Supported":false,"Languages":[{"Code":"en-GB","IsPriorityLanguage":true,"IsSystemLanguage":true,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"en-GB","SubLanguageOf":23,"Id":15,"Title":"English (Great Britain)"},{"Code":"en-US","IsPriorityLanguage":true,"IsSystemLanguage":true,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"en-US","SubLanguageOf":23,"Id":2,"Title":"English (USA)"},{"Code":"en-AU","IsPriorityLanguage":false,"IsSystemLanguage":false,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"en-AU","SubLanguageOf":23,"Id":90,"Title":"English (Australia)"},{"Code":"en-CA","IsPriorityLanguage":false,"IsSystemLanguage":false,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"en-CA","SubLanguageOf":23,"Id":88,"Title":"English (Canada)"},{"Code":"en-IE","IsPriorityLanguage":false,"IsSystemLanguage":false,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"en-IE","SubLanguageOf":23,"Id":20,"Title":"English (IE)"},{"Code":"en-nz","IsPriorityLanguage":false,"IsSystemLanguage":false,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"en-nz","SubLanguageOf":23,"Id":99,"Title":"English (New Zealand)"},{"Code":"en-za","IsPriorityLanguage":false,"IsSystemLanguage":false,"EnabledTargetLanguage":true,"EnabledSourceLanguage":false,"SdlCode":"en-za","SubLanguageOf":23,"Id":100,"Title":"English (South Africa)"}]}';
        break;
    }

    return new Response(200, [], $data);
  }

  /**
   * Returns "true" or "false" if the request was done with a correct.
   *
   * @param \GuzzleHttp\Psr7\Request $request
   *
   * @return \GuzzleHttp\Psr7\Response
   */
  protected function accountCheck(Request $request) {
    $headers = $request->getHeaders();

    if (!empty($headers['Authorization']) && $headers['Authorization'][0] == 'Basic ' . base64_encode("testing@test.com:da422948-b6c7-4a64-8e14-d7f12889babc")) {
      return new Response(200,[], json_encode(TRUE));
    }
    else {
      return new Response(200,[], json_encode(FALSE));
    }
  }

  /**
   * Returns an example Quote JSON as Supertext does it.
   */
  protected function quote() {
    $data = '{"AccountValid":false,"Currency":"EUR","Options":[{"DeliveryOptions":[{"DeliveryDate":"2012-05-03T12:09:46.0000000Z","DeliveryId":1,"Name":"6h","Price":86},{"DeliveryDate":"2012-05-03T12:09:46.0000000Z","DeliveryId":2,"Name":"24h","Price":77},{"DeliveryDate":"2012-05-03T12:09:46.0000000Z","DeliveryId":3,"Name":"48h","Price":67},{"DeliveryDate":"2012-05-03T12:09:46.0000000Z","DeliveryId":4,"Name":"3 Days","Price":57},{"DeliveryDate":"2012-05-03T12:09:46.0000000Z","DeliveryId":5,"Name":"1 Week","Price":48}],"Description":"<p>For an exact reproduction of the original text, the translated text is checked by a proofreader.<\/p>","Name":"Translation","OrderTypeId":6,"ShortDescription":"4-eye principle."},{"DeliveryOptions":[{"DeliveryDate":"2012-05-03T12:09:46.0000000Z","DeliveryId":2,"Name":"24h","Price":178},{"DeliveryDate":"2012-05-03T12:09:46.0000000Z","DeliveryId":3,"Name":"48h","Price":155},{"DeliveryDate":"2012-05-03T12:09:46.0000000Z","DeliveryId":4,"Name":"3 Days","Price":133},{"DeliveryDate":"2012-05-03T12:09:46.0000000Z","DeliveryId":5,"Name":"1 Week","Price":111}],"Description":"<p>For the translation to sound as good as the original, the translated text is stylistically post-edited.<\/p>","Name":"Adaptation","OrderTypeId":7,"ShortDescription":"6-eye principle."}],"WordCount":4}';
    return new Response(200, [], $data);
  }

  /**
   * Returns an example Order JSON as Supertext does it.
   */
  protected function order(Request $request) {
    if ($request->getMethod() !== 'POST') {
      throw new AccessDeniedHttpException();
    }

    $data = '[{"AdditionalInformation":null,"Background":null,"CallbackUrl":null,"ContentType":null,"Currency":"CHF","Deadline":"2012-05-03T12:09:46.0000000Z","DeliveryId":1,"Files":null,"Groups":[],"Id":16516,"Links":null,"OrderDate":"2012-05-03T12:09:46.0000000Z","OrderName":null,"OrderType":"Translation","OrderTypeId":6,"Price":124,"ReferenceData":"1:792f705b290c2efebab95c557f19e153","Referrer":null,"SourceLang":"en","Status":"New","StyleTonality":null,"TargetAudience":null,"TargetLang":"de-CH","WordCount":4}]';
    return new Response(200, [], $data);
  }

}
