<?php

namespace Drupal\Tests\tmgmt_supertext\Functional;

use Drupal\Tests\tmgmt\Functional\TMGMTTestBase;
use Drupal\tmgmt\Translator\AvailableResult;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;

/**
 * Tests tmgmt supertext.
 *
 * @group tmgmt_supertext
 */
class TMGMTSupertextTest extends TMGMTTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['tmgmt_supertext', 'tmgmt_supertext_test'];

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    // Login as admin to be able to set environment variables.
    $this->loginAsAdmin();
    $this->addLanguage('de');
    $this->addLanguage('es');
    $this->addLanguage('it');
    $this->addLanguage('ru');

    // We are testing with UTC Timezone and not the one from the Server.
    \Drupal::configFactory()->getEditable('system.date')
      ->set('timezone.user.configurable', FALSE)
      ->set('timezone.user.default', 'UTC')
      ->save();
    date_default_timezone_set('UTC');
  }

  /**
   * Tests basic API methods of the plugin.
   */
  public function testSupertext() {
    /** @var \Drupal\tmgmt\Entity\Translator $translator */
    $translator = \Drupal::entityTypeManager()->getStorage('tmgmt_translator')->load('supertext');

    // The Translator is not available without credentials.
    $not_available_message = t('@translator is not available. Make sure it is properly <a href=:configured>configured</a>.', [
      '@translator' => $translator->label(),
      ':configured' => $translator->toUrl(),
    ]);
    $this->assertEquals(AvailableResult::no($not_available_message)->getSuccess(), $translator->checkAvailable()->getSuccess());

    $translator->setSetting('api_username', 'testing@test.com');
    $translator->setSetting('api_token', 'da422948-b6c7-4a64-8e14-d7f12889babc');
    $translator->save();
    $this->assertEquals(AvailableResult::yes()->getSuccess(), $translator->checkAvailable()->getSuccess());

    // Checking the translator Manage page, it should show language mapping
    // selects for all langauges which are not directly supported by Supertext.
    $this->drupalGet('admin/tmgmt/translators/manage/supertext');
    $this->assertSession()->pageTextContains('English (en)');
    $this->assertSession()->pageTextContains('German (de)');
    $this->assertSession()->pageTextContains('Spanish (es)');
    $this->assertSession()->pageTextContains('Italian (it)');
    $this->assertSession()->pageTextContains('Russian (ru)');

    // Submitting Language Mappings and wrong credentials it should show a
    // message that the credentials are wrong.
    $edit = [
      'settings[api_username]' => 'testing_wrong@test.com',
      'settings[api_token]' => 'da422948-b6c7-4a64-8e14-d7f12889babc',
      'remote_languages_mappings[de]' => 'de-CH',
      'remote_languages_mappings[es]' => 'es-AR',
      'remote_languages_mappings[it]' => 'it-IT',
      'remote_languages_mappings[en]' => 'en-GB',
      'settings[currency]' => 'EUR',
    ];
    $this->drupalGet('admin/tmgmt/translators/manage/supertext');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('Invalid supertext email or token');

    // Correct credentials, now no message that credentials are wrong.
    $edit = [
      'settings[api_username]' => 'testing@test.com',
      'settings[api_token]' => 'da422948-b6c7-4a64-8e14-d7f12889babc',
    ];
    $this->drupalGet('admin/tmgmt/translators/manage/supertext');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextNotContains('Invalid supertext email or token');

    // Creating the Job.
    $job = $this->createJob('en', 'ru');
    $job->translator = 'supertext';
    $item = $job->addItem('test_source', 'test', '1');
    $item->save();

    // Checking the SupertextOrderObject.
    /** @var \Drupal\tmgmt_supertext\Plugin\tmgmt\Translator\SupertextTranslator $plugin */
    $plugin = $translator->getPlugin();
    $object = $plugin->generateSupertextOrderObject($job);
    $this->assertEquals('eur', $object->Currency);
    $this->assertEquals('en-AU', $object->SourceLang);
    $this->assertEquals(['ru-RU'], $object->TargetLanguages);
    $this->assertEquals('Text for job item with type test and id 1.', $object->Groups['0']->Items['0']->Content);
    $this->assertEquals('dummy][deep_nesting', $object->Groups['0']->Items['0']->Id);

    $this->drupalGet('admin/tmgmt/jobs/' . $job->id());
    $this->assertSession()->pageTextContains('For an exact reproduction of the original text, the translated text is checked by a proofreader.');
    $this->assertSession()->pageTextContains('05/03/2012 - 22:09');
    $this->assertSession()->pageTextContains('86 EUR');

    // Required for submitting a job.
    $job->set('settings', ['translation_option' => '6-1']);
    $job->save();

    // Submitting job, checking if Job now active and messages are
    // shown to the user.
    $job->requestTranslation();
    $this->assertTrue($job->isActive());
    $messages = $job->getMessages();
    $last_message = end($messages);
    $this->assertEquals('Order successfully submitted. The Id for this order is: <em class="placeholder">16516</em>.', (string) $last_message->getMessage());
    $remote_mappings = $job->getRemoteMappings();
    /** @var \Drupal\tmgmt\RemoteMappingInterface $remote_mapping */
    $remote_mapping = array_shift($remote_mappings);
    $this->assertEquals('16516', $remote_mapping->getRemoteIdentifier1());

    // Preparing of post from supertext, with a wrong ReferenceData Hash.
    $post = new \stdClass();
    $post->ReferenceData = $job->id() . ':i_am_wrong';

    // Calling the callback url from the module.
    $http_client = \Drupal::httpClient();
    try {
      $http_client->request('POST', $object->CallbackUrl, ['body' => json_encode($post)]);
      $this->fail('Callback did not deny access');
    }
    catch (GuzzleException $e) {
      // Should be NoAccess.
      $this->assertEquals(403, $e->getCode());
    }

    // Preparing of post from supertext, now with correct ReferenceData Hash.
    $post = new \stdClass();
    $post->ReferenceData = $job->uuid();
    $post->Groups = $object->Groups;
    $post->Groups['0']->Items['0']->Content = "de-CH_" . $post->Groups['0']->Items['0']->Content;
    $post_json = json_encode($post);

    // Calling the callback url from the module.
    /** @var \Psr\Http\Message\ResponseInterface $result */
    try {
      $response = $http_client->request('POST', $object->CallbackUrl, ['body' => $post_json]);
    }
    catch (ServerException $e) {
      $this->fail((string) $e->getResponse()->getBody());
    }

    // Checking if Response is correct.
    $this->assertEquals(200, $response->getStatusCode());
    $this->assertStringContainsString('Translation successfully imported. Thank you.', (string) $response->getBody());

    // JobItems should be in Review.
    \Drupal::entityTypeManager()->getStorage('tmgmt_job_item')->resetCache();
    foreach ($job->getItems() as $item) {
      $this->assertTrue($item->isNeedsReview());
    }

    // Checking if translation is correctly saved.
    $items = $job->getItems();
    $item = end($items);
    $data = $item->getData();
    $this->assertEquals('Text for job item with type test and id 1.', $data['dummy']['deep_nesting']['#text']);
  }

}
