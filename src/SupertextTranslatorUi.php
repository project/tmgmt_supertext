<?php

namespace Drupal\tmgmt_supertext;

use Drupal\Core\Render\Element;
use Drupal\tmgmt\TranslatorPluginUiBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tmgmt\JobInterface;
use Drupal\user\Entity\User;

/**
 * Supertext translator UI.
 */
class SupertextTranslatorUi extends TranslatorPluginUiBase {

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * SupertextTranslatorUi constructor.
   */
  public function __construct() {
    $this->dateFormatter = \Drupal::service('date.formatter');
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();

    // Base settings.
    $form['api_username'] = [
      '#type' => 'email',
      '#title' => t('Default Supertext email'),
      '#required' => TRUE,
      '#default_value' => $translator->getSetting('api_username'),
      '#description' => t("Please enter the email address of your Supertext account."),
    ];

    $form['api_token'] = [
      '#type' => 'textfield',
      '#title' => t('Default Supertext API Token'),
      '#required' => TRUE,
      '#default_value' => $translator->getSetting('api_token'),
      '#description' => t('Please enter your API Token (You can get it from <a target="_blank" href=":url">@text</a>).', [
        ':url' => 'https://www.supertext.ch/customer/accountsettings',
        '@text' => t('Supertext Account'),
      ]),
    ];

    // Other settings.
    $form['currency'] = [
      '#type' => 'select',
      '#title' => t('Your default currency'),
      '#options' => [
        'EUR' => t('Euro'),
        'CHF' => t('Swiss Franc'),
      ],
      '#default_value' => $translator->getSetting('currency'),
      '#description' => t('In what currency do you want to order.'),
    ];

    // @todo This seems to be a nubmer, but in d7 it's a text field. But only
    // one type actually makes sense for us, translation. Do we even keep this?
    $form['service_type_id'] = [
      '#type' => 'number',
      '#title' => t('Numeric ID for the service type'),
      '#default_value' => $translator->getSetting('service_type_id') ? $translator->getSetting('service_type_id') : 4,
      '#min' => 1,
      '#max' => 4,
      '#step' => 1,
    ];

    $form['use_sandbox'] = [
      '#type' => 'checkbox',
      '#title' => t('Use Sandbox'),
      '#default_value' => $translator->getSetting('use_sandbox'),
    ];

    // Accounts.
    $form['accounts'] = [
      '#type' => 'fieldset',
      '#title' => t('Per-user accounts'),
      '#prefix' => '<div id="tmgmt-supertext-accounts-wrapper">',
      '#suffix' => '</div>',
    ];

    // Remove the add more button from the accounts array.
    // @todo This should be done on submit I beleive.
    // @todo Removing the account wansn't supported I think.
    $accounts = ($translator->getSetting('accounts')) ? $translator->getSetting('accounts') : [];
    if ($accounts) {
      $accounts = array_intersect_key($accounts, array_flip(array_filter(array_keys($accounts), 'is_int')));

      // The account fields count on form state, which is not available on
      // initial page load.
      $account_fields_count = ($form_state->has('account_fields_count')) ? $form_state->get('account_fields_count') : count($accounts);
      for ($i = 0; $i < $account_fields_count; $i++) {
        $account_settings = isset($accounts[$i]) ? $accounts[$i] : [];
        $user = !empty($account_settings['drupal_user']) ? User::load($account_settings['drupal_user']) : NULL;

        $form['accounts'][$i] = [
          '#type' => 'fieldset',
          '#title' => ($user) ? $user->label() : t('New user'),
          '#attributes' => [
            'class' => ['supertext-user-account'],
          ],
        ];

        $form['accounts'][$i]['drupal_user'] = [
          '#type' => 'entity_autocomplete',
          '#title' => t('Drupal user'),
          '#target_type' => 'user',
          '#selection_settings' => ['include_anonymous' => FALSE],
          '#default_value' => $user,
          '#size' => 40,
        ];

        $form['accounts'][$i]['account'] = [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['form-item'],
          ],
        ];

        $form['accounts'][$i]['account']['api_username'] = [
          '#type' => 'email',
          '#title' => t('Supertext email'),
          '#default_value' => !empty($account_settings['account']['api_username']) ? $account_settings['account']['api_username'] : '',
          '#size' => 40,
        ];

        $form['accounts'][$i]['account']['api_token'] = [
          '#type' => 'textfield',
          '#title' => t('Supertext API Token'),
          '#default_value' => !empty($account_settings['account']['api_token']) ? $account_settings['account']['api_token'] : '',
          '#size' => 40,
        ];
      }
    }

    $form['accounts']['add_more'] = [
      '#type' => 'submit',
      '#value' => isset($account_fields_count) ? t('Add more') : t('Add per user configuration'),
      '#submit' => [[get_class($this), 'configurationFormCallbackAddMoreSubmit']],
      '#ajax' => [
        'callback' => [get_class($this), 'configurationFormCallbackAddMoreAjax'],
        'wrapper' => 'tmgmt-supertext-accounts-wrapper',
      ],
      '#limit_validation_errors' => [['accounts']],
    ];

    return $form;
  }

  /**
   * Add more submit callback for accounts.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public static function configurationFormCallbackAddMoreSubmit(array &$form, FormStateInterface $form_state) {
    $visible = Element::getVisibleChildren($form['plugin_wrapper']['settings']['accounts']);
    $account_fields_count = count(array_filter($visible, 'is_int'));
    $account_fields_count++;
    $form_state->set('account_fields_count', $account_fields_count);
    $form_state->setRebuild();
  }

  /**
   * Add more ajax callback for accounts.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public static function configurationFormCallbackAddMoreAjax(array &$form, FormStateInterface $form_state) {
    return $form['plugin_wrapper']['settings']['accounts'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('settings');
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $form_state->getFormObject()->getEntity();
    /** @var \Drupal\tmgmt_supertext\Plugin\tmgmt\Translator\SupertextTranslator $translator_plugin */
    $translator_plugin = $translator->getPlugin();

    // Check the main account.
    $main_credentials = ['api_username' => $values['api_username'], 'api_token' => $values['api_token']];
    if (!$translator_plugin->checkCredentials($translator, $main_credentials)) {
      $form_state->setErrorByName("settings[api_username]", t('Invalid supertext email or token'));
    }

    // Check accounts.
    $accaunts_array_parents = 'settings][accounts]';
    foreach ($values['accounts'] as $key => $account) {
      if ($key !== 'add_more' && is_array($account)) {
        // Clenup empty account fields.
        $is_empty = empty($account['drupal_user']) && empty($account['account']['api_username']) && empty($account['account']['api_token']);
        if ($is_empty) {
          $form_state->unsetValue(['settings', 'accounts', $key]);
          continue;
        }

        // Basic checking.
        if (empty($account['drupal_user']) || is_int($account['drupal_user'])) {
          $form_state->setErrorByName("{$accaunts_array_parents}[$key][drupal_user", t('Invalid drupal user'));
        }

        foreach ($account['account'] as $item_key => $item_value) {
          if (empty($item_value)) {
            $form_state->setErrorByName("{$accaunts_array_parents}[$key][account][$item_key", t('The %key field cannot be empty.', ['%key' => $item_key]));
          }
        }

        // Check upstream when all the above is fixed.
        if (!$form_state->getErrors()) {
          if (!$translator_plugin->checkCredentials($translator, $account['account'])) {
            $form_state->setErrorByName("{$accaunts_array_parents}[$key][account", t('Invalid supertext email or token'));
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function checkoutSettingsForm(array $form, FormStateInterface $form_state, JobInterface $job) {
    /** @var \Drupal\tmgmt\TranslatorInterface $translator */
    $translator = $job->getTranslator();
    /** @var \Drupal\tmgmt_supertext\Plugin\tmgmt\Translator\SupertextTranslator $translator_plugin */
    $translator_plugin = $translator->getPlugin();
    $translator_plugin->setTranslator($translator);

    // @todo This is getting called quite often. We shold probably cache it
    // somehow.
    try {
      $object = $translator_plugin->generateSupertextOrderObject($job);
      $response = $translator_plugin->supertextHttpRequest('translation/quote', 'v1', $translator, $object);
    }
    catch (\Exception $e) {
      $job->addMessage('Getting quote failed with error: %e', ['%e' => $e->getMessage()]);
      $form['error'] = [
        '#markup' => t('Error: culd not fetch quote. Check logs for more information.'),
        '#prefix' => '<div class="messages messages--error">',
        '#suffix' => "</div>",
      ];
      return $form;
    }

    $settings['supertext-job'] = [
      '#type' => 'value',
    ];

    if (!$translator->getSetting('api_username')) {
      $account_status = t("There is no API email set in the Supertext Translation Form. You still can see the quote, but will not be able to submit a job.");
    }
    elseif (!$translator->getSetting('api_token')) {
      $account_status = t("There is no API token set in the Supertext Translation Form. You still can see the quote, but will not be able to submit a job.");
    }
    elseif (!$response->AccountValid) {
      $account_status = t("The entered API email and token are not valid. You still can see the quote, but will not be able to submit a job.");
    }
    if (isset($account_status)) {
      $settings['account-status'] = [
        '#type' => 'item',
        '#title' => t('Account Status'),
        '#markup' => $account_status,
        '#prefix' => '<div class="messages error">',
        '#suffix' => "</div>",
      ];
    }

    // Create translation options.
    if (isset($response->Options)) {
      $options = [];

      foreach ($response->Options as $option) {
        if (!isset($form[(int) $option->OrderTypeId])) {
          $options['option-' . (int) $option->OrderTypeId] = [
            '#type' => 'fieldset',
            '#title' => $option->Name,
            '#description' => strip_tags($option->Description),
            '#open' => TRUE,
          ];
        }

        foreach ($option->DeliveryOptions as $delivery) {
          $delivery_date = date_create($delivery->DeliveryDate)->getTimestamp();
          $radio_value = (int) $option->OrderTypeId . '-' . (int) $delivery->DeliveryId;

          $options['option-' . (int) $option->OrderTypeId][$radio_value] = [
            '#type' => 'radio',
            '#parents' => ['translation_option'],
            '#title' => t('@delivery - Price: @price', [
              '@delivery' => $this->dateFormatter->format($delivery_date, 'short'),
              '@price' => !empty($delivery->Price) ? $delivery->Price . " {$response->Currency}" : t('n/a'),
            ]),
            '#return_value' => $radio_value,
            '#attributes' => ['name' => 'translation_option'],
          ];
        }
      }
      $form['translation_option']['#tree'] = FALSE;
      $form['#process'][] = [self::class, 'processTranslationOption'];
      $form['translation_option'] = $options;
    }

    return $form;
  }

  /**
   * Preprocess for translation options.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return array
   *   The form array.
   */
  public static function processTranslationOption(array $form, FormStateInterface $formState) {
    // Because of the separate translation option radios in the form, we need
    // to set the settings value manually.
    $user_input = $formState->getUserInput();
    if (!empty($user_input['translation_option'])) {
      $formState->setValue('settings', ['translation_option' => $user_input['translation_option']]);
    }
    return $form;
  }

}
