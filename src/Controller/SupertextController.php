<?php

namespace Drupal\tmgmt_supertext\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\tmgmt\JobInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Route controller class for the tmgmt_supertext module.
 */
class SupertextController extends ControllerBase {

  /**
   * Provides a callback function to receive translations from Supertext.
   *
   * @param \Drupal\tmgmt\JobInterface $tmgmt_job
   *   Job entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @throws BadRequestHttpException
   *   If something went wrong while adding translations.
   */
  public function receiveTranslation(JobInterface $tmgmt_job, Request $request) {
    $data = json_decode($request->getContent());

    /** @var \Drupal\tmgmt\Data $data_service */
    $data_service = \Drupal::service('tmgmt.data');

    // Add translated data.
    $translation = [];
    foreach ($data->Groups as $group) {
      foreach ($group->Items as $string_object) {
        $translation[$group->GroupId . $data_service::TMGMT_ARRAY_DELIMITER . $string_object->Id] = [
          '#text' => $string_object->Content,
        ];
      }
    }

    try {
      $job_items = $data_service->unflatten($translation);
      $tmgmt_job->addTranslatedData($job_items);
      $tmgmt_job->save();
    }
    catch (\Exception $e) {
      throw new BadRequestHttpException('Could not add translation data to job.');
    }

    return new Response('Translation successfully imported. Thank you.', 200);
  }

  /**
   * Access callback for receiving translations.
   *
   * @param \Drupal\tmgmt\JobInterface $tmgmt_job
   *   Job entity.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object
   */
  public function accessReceive(JobInterface $tmgmt_job) {
    if (!$tmgmt_job->isActive()) {
      $this->getLogger('tmgmt_supertext')->error('Job with id %id has already been processed.', ['%id' => $tmgmt_job->id()]);
      return AccessResult::forbidden();
    }

    // Check if uuids match.
    // @todo d7 version here checks also if data structure is correct. Not sure if needed.
    // @todo Check if there is a better way to get this.
    $request = \Drupal::request();
    $data = $request->getContent();
    if ($json = json_decode($data)) {
      if ($json->ReferenceData == $tmgmt_job->uuid()) {
        return AccessResult::allowed();
      }

      $this->getLogger('tmgmt_supertext')->error('Mismatched identifier when trying to import job with id %id.', ['%id' => $tmgmt_job->id()]);
    }

    return AccessResult::forbidden();
  }
}
