<?php

namespace Drupal\tmgmt_supertext\Plugin\tmgmt\Translator;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\tmgmt\Data;
use Drupal\tmgmt\TranslatorPluginBase;
use GuzzleHttp\Client;
use stdClass;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\tmgmt\TranslatorInterface;
use Drupal\tmgmt\JobInterface;
use Drupal\tmgmt\Translator\AvailableResult;

/**
 * Supertext translation plugin controller.
 *
 * @TranslatorPlugin(
 *   id = "supertext",
 *   label = @Translation("Supertext translator"),
 *   description = @Translation("Supertext translation service."),
 *   ui = "Drupal\tmgmt_supertext\SupertextTranslatorUi",
 *   logo = "icons/supertext.png",
 * )
 */
class SupertextTranslator extends TranslatorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The translator.
   *
   * @var \Drupal\tmgmt\TranslatorInterface
   */
  protected $translator;

  /**
   * Translation service URL.
   *
   * @var string
   */
  const API_URL = 'https://www.supertext.ch/api/';

  /**
   * Translation service sandbox URL.
   *
   * @var string
   */
  const SANDBOX_URL = 'https://dev.supertext.ch/api/';

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $supertextSettings;

  /**
   * The client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Data service.
   *
   * @var \Drupal\tmgmt\Data
   */
  protected $dataService;

  /**
   * Sets a Translator.
   *
   * @param \Drupal\tmgmt\TranslatorInterface $translator
   *   The translator.
   */
  public function setTranslator(TranslatorInterface $translator) {
    if (!isset($this->translator)) {
      $this->translator = $translator;
    }
  }

  /**
   * Constructs a SupertextTranslator object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger for this channel.
   * @param \GuzzleHttp\Client $http_client
   *   Http client.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current user.
   * @param \Drupal\tmgmt\Data $data_service
   *   The data service.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, LoggerInterface $logger, Client $http_client, AccountProxyInterface $current_user, Data $data_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
    $this->httpClient = $http_client;
    $this->currentUser = $current_user;
    $this->dataService = $data_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory')->get('tmgmnt_supertext'),
      $container->get('http_client'),
      $container->get('current_user'),
      $container->get('tmgmt.data')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function requestTranslation(JobInterface $job) {
    $this->setTranslator($job->getTranslator());

    try {
      $object = $this->generateSupertextOrderObject($job);
      list($order_type_id, $delivery_id) = explode('-', $job->get('settings')->translation_option);
      $object->OrderTypeId = $order_type_id;
      $object->DeliveryId = $delivery_id;
      $object->OrderName = (string) $job->label();

      $response = $this->supertextHttpRequest('translation/order', 'v1.1', $job->getTranslator(), $object);
      $orderId = $response[0]->Id;

      // Save order id to the job.
      /** @var JobItemInterface $job_item */
      foreach ($job->getItems() as $job_item) {
        $job_item->addRemoteMapping(NULL, $orderId);
        $job_item->addMessage('Job item successfully submited. Remote ID is %id.', ['%id' => $orderId], 'debug');
      }
      $job->submitted('Order successfully submitted. The Id for this order is: %id.', ['%id' => $orderId]);
    }
    catch (\Exception $e) {
      $job->rejected('Job has been rejected with following error: @error', ['@error' => $e->getMessage()], 'error');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function abortTranslation(JobInterface $job) {
    $this->messenger()->addStatus(t('Aborting not supported at the moment'));
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedRemoteLanguages(TranslatorInterface $translator) {
    $pairs = [];

    // Instead of trying to get all the languages supertext supports, we just
    // fetch the ones that are enabled on the our side.
    /** @var \Drupal\Core\Language\LanguageInterface $language */
    $enabled = \Drupal::languageManager()->getLanguages();
    foreach ($enabled as $language) {
      try {
        $service = 'translation/languagemapping/' . $language->getId();
        $mapping = $this->supertextHttpRequest($service, 'v1', $translator, NULL, "GET");

        if (!$mapping->Supported) {
          foreach ($mapping->Languages as $match) {
            $pairs[$match->Code] = $match->Title;
          }
        }
        else {
          $pairs[$language->getId()] = $language->getName();
        }
      }
      catch (\Exception $e) {
        $this->logger->error('Error while fetching supported remote languages: %e', ['%e' => $e->getMessage()]);
      }
    }

    return $pairs;
  }

  /**
   * {@inheritdoc}
   */
  public function checkAvailable(TranslatorInterface $translator) {
    if ($this->getCredentials($translator)) {
      return AvailableResult::yes();
    }
    return AvailableResult::no(t('@translator is not available. Make sure it is properly <a href=:configured>configured</a>.', [
      '@translator' => $translator->label(),
      ':configured' => $translator->toUrl()->toString(),
    ]));
  }

  /**
   * Returns Supertext credentials.
   *
   * @param \Drupal\tmgmt\TranslatorInterface $translator
   *   The translator entity.
   * @param bool $show_warnings
   *   A boolean indicating whether warning messages should be shown to the user
   *   in case if account is not set.
   *
   * @return array
   *   Array with api_username and api_token keys and values or empty array if
   *   not set.
   */
  public function getCredentials(TranslatorInterface $translator, $show_warnings = FALSE) {
    // First try to get user-specific account.
    if ($accounts = $translator->getSetting('accounts')) {
      foreach ($accounts as $account) {
        if ($account['drupal_user'] == $this->currentUser->id()) {
          return [
            'api_username' => $account['account']['api_username'],
            'api_token' => $account['account']['api_token'],
          ];
        }
      }
    }

    // Fallback to the default account.
    $api_username = trim($translator->getSetting('api_username'));
    $api_token = trim($translator->getSetting('api_token'));

    if (!empty($api_username) && !empty($api_token)) {
      return [
        'api_username' => $api_username,
        'api_token' => $api_token,
      ];
    }

    // No credentials.
    if ($show_warnings) {
      $message = t('To submit orders, you need to <a href=":url">setup your Supertext email address and API token</a>.', [
        ':url' => Url::fromRoute('entity.tmgmt_translator.edit_form', ['tmgmt_translator' => $translator->id()])->toString(),
      ]);
      $this->messenger()->addWarning($message, FALSE);
    }

    return [];
  }

  /**
   * Validates Supertext credentials.
   *
   * @param \Drupal\tmgmt\TranslatorInterface $translator
   *   The translator entity.
   * @param array $credentials
   *   An array with credentials to use. The array format should be:
   *   array("api_username" => {username-string}, "api_token" => {token-string})
   *
   * @return bool
   *   True if check has succeded.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function checkCredentials(TranslatorInterface $translator, array $credentials) {
    try {
      $result = $this->supertextHttpRequest('accountcheck', 'v1', $translator, NULL, 'GET', $credentials);

      if (!$result) {
        throw new \Exception(sprintf('User (%s) or token (%s) not valid', $credentials['api_username'], $credentials['api_token']));
      }
      return TRUE;
    }
    catch (\Exception $e) {
      $this->logger->alert('Checking credentials failed with the following reason: %e', ['%e' => $e->getMessage()]);
      return FALSE;
    }
  }

  /**
   * Generates the Order Object needed by Supertext.
   *
   * @param \Drupal\tmgmt\JobInterface $job
   *   The TMGMT Job Object to make the Order for.
   *
   * @return object
   *   The supertext order object.
   *
   * @throws \Drupal\tmgmt\TMGMTException
   */
  public function generateSupertextOrderObject(JobInterface $job) {
    $translator = $job->getTranslator();
    $source_language = $job->getRemoteSourceLanguage();
    $target_language = $job->getRemoteTargetLanguage();

    $object = new stdClass();
    $object->CallbackUrl = Url::fromRoute('tmgmt_supertext.callback', ['tmgmt_job' => $job->id()], ['absolute' => TRUE])->toString();
    $object->SystemName = "Drupal";
    $object->SystemVersion = \Drupal::VERSION;
    $object->ComponentName = "TMGMT";
    $object->Currency = strtolower($translator->getSetting('currency'));
    $object->ContentType = 'text/html';
    $object->SourceLang = $source_language;
    $object->TargetLanguages = [$target_language];
    $object->ReferenceData = $job->uuid();
    $object->Referrer = \Drupal::config('system.site')->get('name');
    $object->Groups = [];

    // Add ServiceTypeId if it is defined and is integer.
    $service_type_id = trim($translator->getSetting('service_type_id'));
    if (ctype_digit($service_type_id)) {
      $object->ServiceTypeId = (int) $service_type_id;
    }

    /** @var \Drupal\tmgmt\JobItemInterface $tjiid */
    foreach ($job->getItems() as $tjiid => $job_item) {
      $group = new stdClass();
      $group->GroupId = $tjiid;
      $group->Context = $job_item->label();
      $group->Items = [];

      $fields = $this->dataService->filterTranslatable($job_item->getData());
      foreach ($fields as $fieldarray_id => $field_array) {
        $field = new stdClass();
        $field->Content = $field_array['#text'];
        $field->Context = $this->dataService->itemLabel($field_array);
        $field->Id = $fieldarray_id;
        $group->Items[] = $field;
      }
      $object->Groups[] = $group;
    }
    return $object;
  }

  /**
   * General function for making an HTTP Request to Supertext API.
   *
   * @param string $service
   *   The service to be called, the API URL itself.
   * @param string $api
   *   The api variant number (v1 or v1.1)
   * @param \Drupal\tmgmt\TranslatorInterface $translator
   *   Object of the translator which should be used.
   * @param object $dataobject
   *   The object to be sent via POST.
   * @param string $method
   *   With HTTP Method should be used. Default to POST.
   * @param array|null $credentials
   *   An optional array with credentials to use. The array format should be:
   *   array("api_username" => {username-string}, "api_token" => {token-string})
   *
   * @return object
   *   Returns of drupal_http_request()
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @throws \Exception
   */
  public function supertextHttpRequest($service, $api, TranslatorInterface $translator, $dataobject = NULL, $method = "POST", $credentials = NULL) {
    if (!$credentials) {
      $credentials = $this->getCredentials($translator, TRUE);

      if (empty($credentials)) {
        throw new \Exception(t('No credentials found.'));
      }
    }

    $url = $translator->getSetting('use_sandbox') ? self::SANDBOX_URL . $api . '/' : self::API_URL . $api . '/';

    $options['headers'] = [
      'User-Agent' => 'Drupal Supertext TMGMT module 8.x',
      'Authorization' => 'Basic ' . base64_encode($credentials['api_username'] . ':' . $credentials['api_token']),
      'Content-Type' => 'text/json',
    ];

    if (isset($dataobject)) {
      $options['json'] = $dataobject;
    }

    $response = $this->httpClient->request($method, $url . $service, $options);

    return json_decode($response->getBody()->getContents());
  }

}
